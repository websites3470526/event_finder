request = new XMLHttpRequest();
var userId = 0;
var lat;
var long;

//Function to send ajax requests.
let ajaxRequest = (method, url, data, callback) => {
	
	var request = new XMLHttpRequest();
	request.open(method, url);
	
	if(method == "POST"){
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		
	}
	
	request.onload = function(){
		var response = request.responseText;
		callback(response);
	}
	request.send(data);

}

//Function to send jsonRequests
let jsonRequest = (url, data, callback) =>{
	
	request.open("POST", url);
	request.setRequestHeader('Content-Type', 'application/json');
	
	
	request.onload = function(){
		var response = request.responseText;
		callback(response);
	}
	request.send(data);

}

//Callback function that displays the updates made to the database from updateEvent().
let processUpdate = (response) =>{
	var display = document.getElementById("updateMessage");
	display.innerHTML = response;

}

//Callback function that displays the events list from getEvent().
let processResult = (response) =>{
	var display = document.getElementById("data");
	
	display.innerHTML = response;
}

//callback function that displays the response from moreInfo().
let processInfo = (response) =>{
	
	var display = document.getElementById("moreInfo");
	display.innerHTML = response;
	
}

//Gets the event list using a ajax request when page is loaded.
let getEvent = () =>{
	url = "getEvent.php"
	ajaxRequest("GET", url, "", processResult);
}
window.load = getEvent();

 //Refreshes page when refresh button is clicked.
let refresh = () => {
	getEvent();
	moreInfo(window.userId);
}

//Gets more information using ajax request from selected events ID.
let moreInfo = (id) => {
    window.userId = id;

    if(window.userId != ""){
    	var url = "getInfo.php";
    	let text = "text=" + window.userId;
    	
    	ajaxRequest("POST", url, text, processInfo);

    }
}

//Gets lat and long of selected event.
let getLatLong = (latLong) =>{
	const text = latLong;
    	const myArray = text.split(", ");
    	window.lat = myArray[0];
    	window.long = myArray[1];
}

//Updates the notes for selected event using event id and text in input box.
let updateEvent = () =>{
	var url = "updateNotes.php";
	var id = window.userId;
	var notes = document.getElementById("notesInput").value;
	
	
	let data = JSON.stringify({
		userId: id,
		notesE: notes
	});
	
	jsonRequest(url, data, processUpdate);
	
	
	
}

 //Gets the weather information from the api.
getWeather = () =>{
	fetch("https://api.openweathermap.org/data/2.5/weather?lat="+window.lat+"&lon="+window.long+"&appid=6a29afe1c301b3e73345c9184a8846c5&units=metric")
	
	.then(response=> response.text())
	.then(showWeather);
}

//Parses the JSON response from the weather api and displays information that is selected.
showWeather = (response) =>{
	txt = document.getElementById('weatherUpdate');
	
	let data = JSON.parse(response);
	var temp = data.main.temp
	var description = data.weather[0].description;
	txt.innerHTML = temp + "°C with " + description;
}



  





